<h1>Enunciado</h1>
Para mejorar la experiencia del cliente, la sucursal default que se ofrece para store pickup debe ser la más
cercana a su ubicación. Para esto, una de las funcionalidades que se necesita es conocer la sucursal más cercana
a un punto dado (latitud y longitud).
Diseñar e implementar un servicio que exponga en su API las operaciones CRUD (únicamente creación y
lectura por id) de la entidad Sucursal y la consulta de sucursal más cercana a un punto dado.
Los campos de dicha entidad son id , dirección , latitud y longitud .
Diseñar e implementar (unit tests) al menos UN caso de prueba exitoso y uno no exitoso de dominio para la
operación de creación de una sucursal.
Diseñar e implementar al menos UN unit test exitoso del cálculo de distancias.
<h1>Api</h1>
GET /healtcheck
<br>
Descripcion: Se utiliza para verificar que la aplicacion este corriendo
<br>
Retorna: Status: 200
<br>
<br>
POST /branch
<br>
Descripcion: Se utiliza para cargar una nueva sucursal
<br>
Request: src/main/kotlin/com/fravega/challenge/models/BranchRequestDTO
<br>
Retorna:
<br> 
Status: 201 Body: src/main/kotlin/com/fravega/challenge/models/BranchResponseDTO 
<br>
<br>
GET /branch/{branchId}
<br>
Retorna:
<br> 
Status: 200 Body: src/main/kotlin/com/fravega/challenge/models/BranchResponseDTO 
<br> 
Status: 404 (Si el no hay ninguna sucursal cargada con ese Id)
<br>
<br>
GET /branch?latitude={latitude}&longitude={longitude}
<br>
Retorna:
<br> 
Status: 200 Body: src/main/kotlin/com/fravega/challenge/models/BranchResponseDTO 
<br> 
Status: 404 (Si el no hay ninguna sucursal cargada)