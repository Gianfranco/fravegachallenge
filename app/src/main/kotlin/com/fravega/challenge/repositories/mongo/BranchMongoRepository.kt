package com.fravega.challenge.repositories.mongo

import com.fravega.challenge.models.Branch
import org.springframework.data.geo.Point
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

interface BranchMongoRepository : MongoRepository<Branch, String> {

    fun findFirstByPositionNear(position: Point): Optional<Branch>
}