package com.fravega.challenge.repositories

import com.fravega.challenge.models.Branch
import com.fravega.challenge.repositories.mongo.BranchMongoRepository
import org.springframework.data.geo.Point
import org.springframework.stereotype.Service

@Service
class BranchRepository(private val branchMongoRepository: BranchMongoRepository) {

    fun find(id: String): Branch? = this.branchMongoRepository.findById(id).orElse(null)

    fun findNearTo(position: Point): Branch? =
            this.branchMongoRepository.findFirstByPositionNear(position).orElse(null)

    fun save(branch: Branch) = this.branchMongoRepository.save(branch)
}

