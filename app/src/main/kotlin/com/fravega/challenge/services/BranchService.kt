package com.fravega.challenge.services

import com.fravega.challenge.models.Branch
import com.fravega.challenge.repositories.BranchRepository
import org.slf4j.LoggerFactory
import org.springframework.data.geo.Point
import org.springframework.stereotype.Service

@Service
class BranchService(private val branchRepository: BranchRepository) {

    private val logger = LoggerFactory.getLogger(javaClass)

    fun save(branch: Branch): Branch {
        logger.info("Saving branch")
        return this.branchRepository.save(branch)
    }

    fun find(id: String): Branch? {
        logger.info("Searching branch with id: $id")
        return this.branchRepository.find(id)
    }

    fun findByProximity(position: Point): Branch? {
        logger.info("Searching branch by proximity")
        return this.branchRepository.findNearTo(position)
    }
}