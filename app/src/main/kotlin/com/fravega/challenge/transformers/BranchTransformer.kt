package com.fravega.challenge.transformers

import com.fravega.challenge.models.Branch
import com.fravega.challenge.models.BranchRequestDTO
import com.fravega.challenge.models.BranchResponseDTO
import org.springframework.data.mongodb.core.geo.GeoJsonPoint

object BranchTransformer {

    fun transform(request: BranchRequestDTO): Branch =
            Branch(
                    address = request.address,
                    position = GeoJsonPoint(request.longitude, request.latitude))


    fun transform(branch: Branch): BranchResponseDTO =
            BranchResponseDTO(
                    id = branch.id!!,
                    address = branch.address,
                    longitude = branch.position.x,
                    latitude = branch.position.y)

}