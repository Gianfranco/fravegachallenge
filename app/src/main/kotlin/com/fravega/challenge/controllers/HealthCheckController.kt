package com.fravega.challenge.controllers

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/healthcheck")
class HealthCheckController {

    @GetMapping
    fun save() = ResponseEntity.status(HttpStatus.CREATED).body("Alive")
}