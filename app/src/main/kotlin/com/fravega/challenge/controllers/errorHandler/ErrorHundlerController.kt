package com.fravega.challenge.controllers.errorHandler

import com.fravega.challenge.models.ErrorMessageResponseDTO
import org.slf4j.LoggerFactory
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ErrorHandlerController {

    private val logger = LoggerFactory.getLogger(javaClass)

    @ExceptionHandler(value = [(Exception::class)])
    fun handlerUnexpectedError(exception: Exception): ResponseEntity<ErrorMessageResponseDTO> {
        logger.error("Unexpected Error: " + exception.message, exception)
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ErrorMessageResponseDTO("Internal Error Server"))
    }

    @ExceptionHandler(value = [(NotFoundException::class)])
    fun handlerNotFoundError() = ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorMessageResponseDTO("Not found element"))
}