package com.fravega.challenge.controllers

import com.fravega.challenge.models.BranchRequestDTO
import com.fravega.challenge.models.BranchResponseDTO
import com.fravega.challenge.services.BranchService
import com.fravega.challenge.transformers.BranchTransformer.transform
import org.slf4j.LoggerFactory
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.OK
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/branches")
class BranchController(private val branchService: BranchService) {

    private val logger = LoggerFactory.getLogger(javaClass)

    @PostMapping
    fun save(@RequestBody request: BranchRequestDTO): ResponseEntity<BranchResponseDTO> {
        val branch = transform(request)
        val branchSaved = this.branchService.save(branch)
        val response = transform(branchSaved)
        return ResponseEntity.status(CREATED).body(response)
    }

    @GetMapping("/{id}")
    fun find(@PathVariable("id") id: String): ResponseEntity<BranchResponseDTO> {
        val branch = this.branchService.find(id)
        return if (branch != null) {
            val response = transform(branch)
            ResponseEntity.status(OK).body(response)
        } else {
            logger.info("No branch found with id : $id")
            throw NotFoundException()
        }
    }

    @GetMapping
    fun findByProximity(@RequestParam longitude: Double, @RequestParam latitude: Double): ResponseEntity<BranchResponseDTO> {
        val branch = this.branchService.findByProximity(GeoJsonPoint(longitude, latitude))
        return if (branch != null) {
            val response = transform(branch)
            ResponseEntity.status(OK).body(response)
        } else {
            logger.info("No branch found")
            throw NotFoundException()
        }

    }
}