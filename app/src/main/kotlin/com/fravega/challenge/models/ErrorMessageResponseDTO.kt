package com.fravega.challenge.models

data class ErrorMessageResponseDTO(val message: String)