package com.fravega.challenge.models

data class BranchRequestDTO(val address: String,
                            val longitude: Double,
                            val latitude: Double)