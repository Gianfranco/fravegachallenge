package com.fravega.challenge.models

data class BranchResponseDTO(val id: String,
                             val address: String,
                             val longitude: Double,
                             val latitude: Double)