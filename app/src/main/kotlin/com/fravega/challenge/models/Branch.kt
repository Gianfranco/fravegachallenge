package com.fravega.challenge.models

import org.springframework.data.annotation.Id
import org.springframework.data.geo.Point
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "branches")
data class Branch(@Id val id: String? = null,
                  val address: String,
                  val position: Point)