package com.fravega.challenge.integration

import com.fravega.challenge.models.BranchRequestDTO
import com.fravega.challenge.models.BranchResponseDTO
import com.fravega.challenge.stubs.BranchStub.getBranchRequestDTO
import com.mongodb.client.model.Indexes
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.boot.test.web.client.postForEntity
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.http.HttpStatus.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BranchIntegrationTest {

    @Autowired
    private lateinit var restTemplate: TestRestTemplate

    @Autowired
    private lateinit var template: MongoTemplate

    @BeforeEach
    fun cleanDatabase() {
        val collection = template.db.getCollection("branches")
        collection.drop()
        collection.createIndex(Indexes.geo2dsphere("position"))
    }

    @Test
    fun saveAndFindSuccess() {
        //given
        val branchSavedResponse = saveBranch(getBranchRequestDTO())
        //when
        val response = this.restTemplate.getForEntity<BranchResponseDTO>(
                url = "/branches/${branchSavedResponse.id}")
        //then
        assertEquals(OK, response.statusCode)
        assertEquals(branchSavedResponse, response.body)
    }

    @Test
    fun findWithoutResults() {
        //given
        saveBranch(getBranchRequestDTO())
        //when
        val response = this.restTemplate.getForEntity<String>(
                url = "/branches/FakeBranchId")
        //then
        assertEquals(NOT_FOUND, response.statusCode)
        assertEquals("""{"message":"Not found element"}""", response.body)
    }

    @Test
    fun findByProximityWithResult() {
        //give
        val nearbyBranch = getBranchRequestDTO(latitude = 0.0, longitude = 1.0)
        val anotherBranch = getBranchRequestDTO(latitude = 1.0, longitude = 0.0)
        val nearbyBranchSaved = saveBranch(nearbyBranch)
        saveBranch(anotherBranch)
        //when
        val response = this.restTemplate.getForEntity<BranchResponseDTO>(
                url = "/branches?latitude=0.0&longitude=0.9")
        //then
        assertEquals(OK, response.statusCode)
        assertEquals(nearbyBranchSaved, response.body)
    }

    @Test
    fun findByProximityWithoutResults() {
        //when
        val response = this.restTemplate.getForEntity<String>(
                url = "/branches?latitude=0.0&longitude=0.0")
        //then
        assertEquals(NOT_FOUND, response.statusCode)
        assertEquals("{\"message\":\"Not found element\"}", response.body)
    }

    @Test
    fun databaseError() {
        //give
        template.db.getCollection("branches").drop()
        saveBranch(getBranchRequestDTO())
        //when
        val response = this.restTemplate.getForEntity<String>(
                url = "/branches?latitude=0.0&longitude=0.0")
        //then
        assertEquals(INTERNAL_SERVER_ERROR, response.statusCode)
        assertEquals("""{"message":"Internal Error Server"}""", response.body)
    }

    private fun saveBranch(request: BranchRequestDTO): BranchResponseDTO {
        val response = restTemplate.postForEntity<BranchResponseDTO>(
                url = "/branches",
                request = request)
        assertEquals(CREATED, response.statusCode)
        assertNotNull(response.body)
        val body = response.body!!
        assertEquals(request.address, body.address)
        assertEquals(request.longitude, body.longitude)
        assertEquals(request.latitude, body.latitude)
        assertNotNull(body.id)
        return body
    }
}