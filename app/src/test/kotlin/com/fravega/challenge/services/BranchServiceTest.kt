package com.fravega.challenge.services

import com.fravega.challenge.repositories.BranchRepository
import com.fravega.challenge.stubs.BranchStub.getBranch
import com.fravega.challenge.stubs.BranchStub.getPosition
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import java.util.*

class BranchServiceTest {

    private lateinit var branchRepository: BranchRepository
    private lateinit var service: BranchService

    @BeforeEach
    fun setUp() {
        this.branchRepository = Mockito.mock(BranchRepository::class.java)
        this.service = BranchService(this.branchRepository)
    }

    @Test
    fun save() {
        //given
        val branch = getBranch(id = null)
        val branchSaved = branch.copy(id = UUID.randomUUID().toString())
        `when`(this.branchRepository.save(branch)).thenReturn(branchSaved)
        //when
        val response = this.service.save(branch)
        //then
        assertEquals(branchSaved, response)
    }

    @Test
    fun findWithResult() {
        //given
        val id = UUID.randomUUID().toString()
        val branch = getBranch(id = id)
        `when`(this.branchRepository.find(id)).thenReturn(branch)
        //when
        val response = this.service.find(id)
        //then
        assertEquals(branch, response)
    }

    @Test
    fun findWithoutResults() {
        //given
        val id = UUID.randomUUID().toString()
        `when`(this.branchRepository.find(id)).thenReturn(null)
        //when
        val response = this.service.find(id)
        //then
        assertNull(response)
    }

    @Test
    fun findBranchByProximityWithResult() {
        //given
        val position = getPosition()
        val branch = getBranch()
        `when`(this.branchRepository.findNearTo(position)).thenReturn(branch)
        //when
        val response = this.service.findByProximity(position)
        //then
        assertEquals(branch, response)
    }


    @Test
    fun findBranchByProximityWithoutResults() {
        //given
        val position = getPosition()
        `when`(this.branchRepository.findNearTo(position)).thenReturn(null)
        //when
        val response = this.service.findByProximity(position)
        //then
        assertNull(response)
    }

}