package com.fravega.challenge.stubs

import com.fravega.challenge.models.Branch
import com.fravega.challenge.models.BranchRequestDTO
import org.springframework.data.geo.Point
import java.util.*
import kotlin.random.Random

object BranchStub {

    fun getBranchRequestDTO(
            latitude: Double = Random.nextDouble(),
            longitude: Double = Random.nextDouble()): BranchRequestDTO =
            BranchRequestDTO(
                    address = UUID.randomUUID().toString(),
                    longitude = longitude,
                    latitude = latitude
            )

    fun getBranch(id: String? = UUID.randomUUID().toString()): Branch =
            Branch(
                    id = id,
                    address = UUID.randomUUID().toString(),
                    position = getPosition()
            )

    fun getPosition() = Point(Random.nextDouble(), Random.nextDouble())
}