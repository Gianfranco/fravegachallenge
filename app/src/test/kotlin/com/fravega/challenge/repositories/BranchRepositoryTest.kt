package com.fravega.challenge.repositories

import com.fravega.challenge.repositories.mongo.BranchMongoRepository
import com.fravega.challenge.stubs.BranchStub.getBranch
import com.fravega.challenge.stubs.BranchStub.getPosition
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.util.*

class BranchRepositoryTest {

    private lateinit var repository: BranchRepository
    private lateinit var branchMongoRepository: BranchMongoRepository

    @BeforeEach
    fun setUp() {
        this.branchMongoRepository = mock(BranchMongoRepository::class.java)
        this.repository = BranchRepository(this.branchMongoRepository)
    }

    @Test
    fun save() {
        //given
        val branch = getBranch(id = null)
        val branchSaved = branch.copy(id = UUID.randomUUID().toString())
        `when`(this.branchMongoRepository.save(branch)).thenReturn(branchSaved)
        //when
        val result = this.repository.save(branch)
        //then
        assertEquals(branchSaved, result)
    }

    @Test
    fun findWithResult() {
        //given
        val branch = getBranch()
        val id = UUID.randomUUID().toString()
        `when`(this.branchMongoRepository.findById(id)).thenReturn(Optional.of(branch))
        //when
        val result = this.repository.find(id)
        //then
        assertEquals(branch, result)
    }

    @Test
    fun findWithoutResults() {
        //given
        val id = UUID.randomUUID().toString()
        `when`(this.branchMongoRepository.findById(id)).thenReturn(Optional.empty())
        //when
        val result = this.repository.find(id)
        //then
        assertEquals(null, result)
    }

    @Test
    fun findByProximityWithResult() {
        //given
        val position = getPosition()
        val branch = getBranch()
        `when`(this.branchMongoRepository.findFirstByPositionNear(position)).thenReturn(Optional.of(branch))
        //when
        val result = this.repository.findNearTo(position)
        //then
        assertEquals(branch, result)
    }

    @Test
    fun findByProximityWithoutResults() {
        //given
        val position = getPosition()
        `when`(this.branchMongoRepository.findFirstByPositionNear(position)).thenReturn(Optional.empty())
        //when
        val result = this.repository.findNearTo(position)
        //then
        assertEquals(null, result)
    }

}