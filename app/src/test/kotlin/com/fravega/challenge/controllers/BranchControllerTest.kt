package com.fravega.challenge.controllers

import com.fravega.challenge.models.Branch
import com.fravega.challenge.models.BranchResponseDTO
import com.fravega.challenge.services.BranchService
import com.fravega.challenge.stubs.BranchStub
import com.fravega.challenge.stubs.BranchStub.getBranch
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.springframework.data.crossstore.ChangeSetPersister.*
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.http.HttpStatus.*
import java.util.*
import kotlin.random.Random

class BranchControllerTest {

    private lateinit var controller: BranchController
    private lateinit var branchService: BranchService

    @BeforeEach
    fun setUp() {
        this.branchService = Mockito.mock(BranchService::class.java)
        this.controller = BranchController(this.branchService)
    }

    @Test
    fun successSaved() {
        //given
        val request = BranchStub.getBranchRequestDTO()
        val branch = Branch(
                address = request.address,
                position = GeoJsonPoint(request.longitude, request.latitude))
        val branchResponse = branch.copy(id = UUID.randomUUID().toString())
        `when`(this.branchService.save(branch)).thenReturn(branchResponse)
        //when
        val response = this.controller.save(request)
        //then
        val responseExpected = BranchResponseDTO(
                id = branchResponse.id!!,
                address = branchResponse.address,
                longitude = branchResponse.position.x,
                latitude = branchResponse.position.y)
        assertEquals(CREATED, response.statusCode)
        assertEquals(responseExpected, response.body)
    }

    @Test
    fun findWithResult() {
        //given
        val id = UUID.randomUUID().toString()
        val branch = getBranch(id = id)
        `when`(this.branchService.find(id)).thenReturn(branch)
        //when
        val response = this.controller.find(id)
        //then
        val responseExpected = BranchResponseDTO(
                id = id,
                address = branch.address,
                longitude = branch.position.x,
                latitude = branch.position.y)
        assertEquals(OK, response.statusCode)
        assertEquals(responseExpected, response.body)
    }

    @Test
    fun findWithoutResults() {
        //given
        val id = UUID.randomUUID().toString()
        `when`(this.branchService.find(id)).thenReturn(null)
        //when
        Assertions.assertThrows(NotFoundException::class.java) {
            this.controller.find(id)
        }
    }

    @Test
    fun findBranchByProximityWithResult() {
        //given
        val latitude = Random.nextDouble()
        val longitude = Random.nextDouble()
        val branch = getBranch(id = UUID.randomUUID().toString())
        `when`(this.branchService.findByProximity(GeoJsonPoint(latitude, longitude))).thenReturn(branch)
        //when
        val response = this.controller.findByProximity(latitude, longitude)
        //then
        val responseExpected = BranchResponseDTO(
                id = branch.id!!,
                address = branch.address,
                longitude = branch.position.x,
                latitude = branch.position.y)
        assertEquals(OK, response.statusCode)
        assertEquals(responseExpected, response.body)
    }


    @Test
    fun findBranchByProximityWithoutResults() {
        //given
        val latitude = Random.nextDouble()
        val longitude = Random.nextDouble()
        `when`(this.branchService.findByProximity(GeoJsonPoint(latitude, longitude))).thenReturn(null)
        //when
        Assertions.assertThrows(NotFoundException::class.java) {
            this.controller.findByProximity(latitude, longitude)
        }
    }
}