<h1>Run Project</h1>
Para correr todo el proyecto se requiere tener:
<br>
- Docker
<br>
- Docker-compose
<br>
- Java 11
<br>
- Maven
<br>
Para correr el projecto: "docker-compose up" en la raiz del proyecto
<br>
Para informacion de la API del proyecto ver app/README.md